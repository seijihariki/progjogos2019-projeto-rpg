local Character = require "common.class"()

function Character:_init(spec)
  self.spec = spec
  self.hp = spec.max_hp
end

function Character:get_name()
  return self.spec.name
end

function Character:get_appearance()
  return self.spec.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:get_behavior()
  return self.spec.behavior
end

function Character:get_stat_power()
  return self.spec.stats.power
end

function Character:get_stat_resistance()
  return self.spec.stats.resistance
end

function Character:get_stat_hit_chance()
  return self.spec.stats.hit_chance
end

function Character:get_skills()
  return self.spec.skills
end

function Character:apply_effects(effects)
  if not effects then
    return
  end

  if effects.hp then
    self.hp = math.min(self.hp + effects.hp, self.spec.max_hp)
  end
end

function Character:attack(character)
  if character == nil then
    print("Cannot attack nothing")
    return nil
  end

  local damage = self:get_stat_power() + math.random(-1, 1)

  if math.random(1, 100) > self:get_stat_hit_chance() then
    damage = 0
  end

  local res, dealt = character:take_dmg(damage)

  return {dmg = damage, res = res, dealt = dealt}
end

function Character:take_dmg(damage)
  local resist = self:get_stat_resistance() + math.random(-1, 1)
  local dmg = math.floor(damage - resist / 2)

  local actual_dmg = math.min(self.hp, math.max(0, dmg))
  self.hp = self.hp - actual_dmg
  return resist, actual_dmg
end

return Character
