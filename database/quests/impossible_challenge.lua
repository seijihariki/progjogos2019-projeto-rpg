return {
  title = "Impossible Challenge",
  party = {"queen", "priest", "queen"},
  events = {
    {type = "story", text = "You will face the most powerful being of the universe!"},
    {type = "story", text = "Good luck... (you will fail!)"},
    {type = "encounter", enemies = {"chicken"}, runnable = false}
  }
}
