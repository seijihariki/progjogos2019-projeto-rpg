
return {
  title = 'Slime Infestation',
  party = { 'warrior', 'archer', 'priest' },
  events = {
    {type = "encounter", enemies = { 'green_slime' }},
    {type = "encounter", enemies = { 'green_slime', 'green_slime', 'green_slime' }},
    {type = "encounter", enemies = { 'blue_slime', 'green_slime' }},
  }
}

