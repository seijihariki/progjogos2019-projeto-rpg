
return {
  title = 'Halloween',
  party = { 'warrior', 'queen', 'priest' },
  events = {
    {type = "story", text = "It's a dark October night... When suddenly"},
    {type = "encounter", enemies = { 'ghost' }},
    {type = "story", text = "You continue your candy hunt."},
    {type = "story", text = "But the terror is not over yet..."},
    {type = "encounter", enemies = { 'ghost', 'green_slime'}},
    {type = "story", text = "Prepare to face a nightmare!"},
    {type = "encounter", enemies = { 'green_slime', 'ghost', 'blue_slime' }},
  }
}
