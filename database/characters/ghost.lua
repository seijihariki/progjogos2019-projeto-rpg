
return {
  name = "Ghost",
  appearance = 'ghost',
  max_hp = 20,
  behavior = 'more_power',
  stats = {
    power = 4,
    resistance = 4,
    hit_chance = 100
  }
}
