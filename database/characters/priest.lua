return {
  name = "Friendly Priest",
  appearance = "priest",
  max_hp = 8,
  behavior = "less_hp",
  stats = {
    power = 2,
    resistance = 2,
    hit_chance = 100
  },
  skills = {
    {
      name = "Heal",
      type = "support",
      effects = {
        target = {hp = 5}
      },
      target = {
        type = "any",
        amount = 1
      }
    },
    {
      name = "Party Heal",
      type = "support",
      effects = {
        target = {hp = 2}
      },
      target = {
        type = "party",
        amount = -1 -- Minus one means all members
      }
    }
  }
}
