return {
  name = "Blue Slime",
  appearance = "blue_slime",
  max_hp = 16,
  behavior = "less_hp",
  stats = {
    power = 6,
    resistance = 4,
    hit_chance = 70
  }
}
