return {
  name = "Recluse Archer",
  appearance = "archer",
  max_hp = 12,
  behavior = "more_power",
  stats = {
    power = 4,
    resistance = 3,
    hit_chance = 70
  },
  skills = {
    {
      name = "Headshot",
      type = "combat",
      effects = {
        char = {
          stats = {
            power = 5,
            hit_chance = -40
          }
        }
      },
      target = {
        type = "enemy",
        amount = 1
      }
    },
    {
      name = "Multishot",
      type = "combat",
      effects = {
        char = {
          stats = {
            power = -2,
            hit_chance = -30
          }
        }
      },
      target = {
        type = "enemy",
        amount = 2
      }
    }
  }
}
