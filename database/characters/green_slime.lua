return {
  name = "Green Slime",
  appearance = "slime",
  max_hp = 6,
  behavior = "more_hp",
  stats = {
    power = 3,
    resistance = 2,
    hit_chance = 70
  }
}
