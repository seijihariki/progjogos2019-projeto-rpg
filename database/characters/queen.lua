return {
  name = "Fearless Queen",
  appearance = "queen",
  max_hp = 25,
  behavior = "less_hp",
  stats = {
    power = 7,
    resistance = 5,
    hit_chance = 100
  },
  skills = {
    {
      name = "Termination",
      type = "combat",
      effects = {
        char = {
          stats = {
            power = 15,
            hit_chance = -20
          }
        }
      },
      target = {
        type = "enemy",
        amount = 1
      }
    }
  }
}
