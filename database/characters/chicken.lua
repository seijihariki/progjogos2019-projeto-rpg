
return {
  name = "Undying Chicken",
  appearance = 'chicken',
  max_hp = 200,
  behavior = 'less_hp',
  stats = {
    power = 50,
    resistance = 50,
    hit_chance = 100
  }
}
