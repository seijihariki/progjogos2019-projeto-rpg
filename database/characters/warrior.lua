return {
  name = "Veteran Warrior",
  appearance = "knight",
  max_hp = 20,
  behavior = "more_hp",
  stats = {
    power = 6,
    resistance = 4,
    hit_chance = 80
  },
  skills = {
    {
      name = "Charge",
      type = "combat",
      effects = {
        char = {
          stats = {
            power = -3,
            hit_chance = -10
          }
        }
      },
      target = {
        type = "enemy",
        amount = -1
      }
    }
  }
}
