local Msg = {}

function Msg.attacked(msg, character, attacked, attack_data)
  local attacked_msg = character:get_name() .. " attacked " .. attacked:get_name()

  if attack_data.dmg == 0 then
    msg:set(attacked_msg .. " and missed!")
  else
    msg:set(attacked_msg .. " and dealt " .. attack_data.dealt .. " damage")
  end
end

function Msg.used_skill(msg, character, targets, skill)
  local skill_msg = character:get_name() .. " used " .. skill.name

  if skill.target.amount < 0 then
    if skill.target.type == "any" then
      msg:set(skill_msg .. " on everyone!")
    elseif skill.target.type == "party" then
      msg:set(skill_msg .. " on all party members!")
    elseif skill.target.type == "enemy" then
      msg:set(skill_msg .. " on all enemies!")
    end
  else
    skill_msg = skill_msg .. " on " .. #targets

    if skill.target.type == "any" then
      msg:set(skill_msg .. " targets!")
    elseif skill.target.type == "party" then
      msg:set(skill_msg .. " party members!")
    elseif skill.target.type == "enemy" then
      msg:set(skill_msg .. " enemies!")
    end
  end
end

return Msg
