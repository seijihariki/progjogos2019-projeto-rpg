local Vec = require "common.vec"
local MessageBox = require "view.message_box"
local State = require "state"

local StoryState = require "common.class"(State)

function StoryState:_init(stack)
  self:super(stack)

  self.message = nil
  self.continue = nil
end

function StoryState:enter(params)
  local message = MessageBox(Vec())
  local continue = MessageBox(Vec())

  params = params or {}

  self:view():add("message", message)
  self:view():add("continue", continue)

  message:set(params.text or "No text found...")
  message:center()

  continue:set("Press any key to continue")
  continue:set_position(Vec(0, love.graphics.getHeight() - (continue:get_height() + 30)))
  continue:center_h()
end

function StoryState:leave()
  self:view():remove("message")
  self:view():remove("continue")
end

function StoryState:on_keypressed(_)
  self:pop({action = "continue_quest"})
end

return StoryState
