local map = require "common.map"
local ListMenu = require "view.list_menu"
local State = require "state"

local PlayerTurnSkillState = require "common.class"(State)

function PlayerTurnSkillState:_init(stack)
  self:super(stack)
  self.character = nil
  self.menu = nil
  self.parties = nil
end

function PlayerTurnSkillState:enter(params)
  self.character = params.current_character
  self.parties = params.parties
  self.skill = nil

  self.menu_items =
    map(
    self.character:get_skills(),
    function(skill)
      return skill.name
    end
  )

  table.insert(self.menu_items, " Back")

  self.menu = ListMenu(self.menu_items)

  self:_show_menu()
end

function PlayerTurnSkillState:_show_menu()
  local bfbox = self:view():get("battlefield").bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add("menu", self.menu)
end

function PlayerTurnSkillState:leave()
  self:view():remove("menu")
end

local function add_stats(character, char_stat_effects, multi)
  if not character or not char_stat_effects then
    return
  end

  for stat, delta in pairs(char_stat_effects) do
    character.stats[stat] = character.stats[stat] + delta * (multi or 1)
  end
end

function PlayerTurnSkillState:resume(params)
  if params then
    local skill = self.character:get_skills()[self.menu:current_option()]
    local character = self.character
    local targets = params.selected

    local char_stat_effects = skill.effects.char and skill.effects.char.stat
    local target_stat_effects = skill.effects.target and skill.effects.target.stat

    -- Add stats for targets during processing
    add_stats(character, char_stat_effects)
    character:apply_effects(skill.effects.char)

    for _, target in ipairs(targets) do
      add_stats(target, target_stat_effects)

      if skill.type == "combat" then
        character:attack(target)
      elseif skill.type == "support" then
        target:apply_effects(skill.effects.target)
      end
    end

    -- Undo stats modifications
    add_stats(character, char_stat_effects, -1)
    for _, target in ipairs(targets) do
      add_stats(target, target_stat_effects, -1)
    end

    self:pop(
      {
        targets = targets,
        skill = skill
      }
    )
  else
    self:_show_menu()
  end
end

function PlayerTurnSkillState:on_keypressed(key)
  if key == "down" then
    self.menu:next()
  elseif key == "up" then
    self.menu:previous()
  elseif key == "return" then
    -- Go back if last option is selected
    if self.menu:current_option() > #self.character:get_skills() then
      return self:pop(nil)
    end

    local targets = {}
    local skill = self.character:get_skills()[self.menu:current_option()]

    local t_type = skill.target.type -- Type of affected unit
    local t_amount = skill.target.amount -- Amount of affected units

    if t_type == "any" then
      for _, party in pairs(self.parties) do
        for _, character in pairs(party) do
          table.insert(targets, character)
        end
      end
    else
      targets = self.parties[t_type]
    end

    self:push(
      "player_turn_target",
      {
        current_character = self.character,
        targets = targets,
        amount = t_amount
      }
    )
  end
end

return PlayerTurnSkillState
