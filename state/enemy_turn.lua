local State = require "state"
local EnemyTurnState = require "common.class"(State)

local Msg = require "common.msg"

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.targets = nil
  self.target = nil
  self.message = nil
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.targets = params.parties.party
  self.message = params.message

  self:_select_target()

  local attack_data = self.character:attack(self.target)
  Msg.attacked(self.message, self.character, self.target, attack_data)

  self:pop({character = self.character, target = self.target})
end

function EnemyTurnState:_select_target()
  local behavior = self.character:get_behavior()
  self.target = self.targets[1] --chooses first party member as default target
  --Choose target according to one of the behaviors:
  if behavior == "more_hp" then
    local max_hp
    for i, trgt in ipairs(self.targets) do
      local hp, _ = trgt:get_hp()
      max_hp, _ = self.target:get_hp()
      if hp > max_hp then
        self.target = self.targets[i]
      end
    end
  elseif behavior == "less_hp" then
    local min_hp
    for i, trgt in ipairs(self.targets) do
      local hp, _ = trgt:get_hp()
      min_hp, _ = self.target:get_hp()
      if hp < min_hp then
        self.target = self.targets[i]
      end
    end
  elseif behavior == "more_power" then
    local max_power
    for i, trgt in ipairs(self.targets) do
      local power = trgt:get_stat_power()
      max_power = self.target:get_stat_power()
      if power > max_power then
        self.target = self.targets[i]
      end
    end
  end
end

return EnemyTurnState
