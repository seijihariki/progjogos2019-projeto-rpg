local Vec = require "common.vec"
local MessageBox = require "view.message_box"
local SpriteAtlas = require "view.sprite_atlas"
local BattleField = require "view.battlefield"
local State = require "state"

local EncounterState = require "common.class"(State)

local CHARACTER_GAP = 96

function EncounterState:_init(stack)
  self:super(stack)

  self.atlas = nil

  self.turns = nil
  self.next_turn = nil
  self.players = nil --will contain party and their turn order
  self.enemies = nil --will contain encounter and their turn order
  self.runnable = nil
end

function EncounterState:enter(params)
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local n = 0
  local party_origin = battlefield:east_team_origin()

  self.atlas = SpriteAtlas()
  self.turns = {}
  self.players = {} --initializes player vector
  self.runnable = (params.runnable ~= false)
  self.next_turn = 1
  for i, character in ipairs(params.party) do
    if character:get_hp() > 0 then --party member is alive
      local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
      self.turns[i] = character
      self.players[i] = character --add character to player list
      self.atlas:add(character, pos, character:get_appearance())
    else --party member is dead
      self.turns[i] = nil
      self.players[i] = nil --add character to player list
    end
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  self.enemies = {} --initializes enemies vector
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    self.enemies[i] = character --add character to enemies list
    self.atlas:add(character, pos, character:get_appearance())
  end
  self:view():add("atlas", self.atlas)
  self:view():add("battlefield", battlefield)
  self:view():add("message", message)
  message:set("You stumble upon an encounter")
end

function EncounterState:leave()
  self:view():get("atlas"):clear()
  self:view():remove("atlas")
  self:view():remove("battlefield")
  self:view():remove("message")
end

function EncounterState:update(_)
  -- Remove dead characters
  for i, character in ipairs(self.enemies) do -- Remove dead Enemies
    if character:get_hp() == 0 then
      table.remove(self.enemies, i)
      self.atlas:rm(character)
    end
  end

  for i, character in ipairs(self.players) do -- Remove dead Player characters
    if character:get_hp() == 0 then
      table.remove(self.players, i)
      self.atlas:rm(character)
    end
  end

  for i, character in ipairs(self.turns) do -- Remove all dead characters
    if character:get_hp() == 0 then
      table.remove(self.turns, i)
    end
  end

  if next(self.enemies) == nil then --all enemies are dead
    return self:pop({action = "continue_quest", character = self.character}) --go to next encounter
  end

  if next(self.players) == nil then --all players are dead
    return self:pop({action = "game_over"}) --go to main menu
  end

  local current_character = self.turns[self.next_turn]
  if current_character then --current character still alive
    local params = {
      current_character = current_character,
      message = self:view():get("message"),
      -- All characters, divided by category
      parties = {enemy = self.enemies, party = self.players}
    }

    local ret

    if self.next_turn > #self.players then
      --enemies turn
      if self.next_turn == #self.turns then --last enemy
        self.next_turn = 1
      else
        self.next_turn = self.next_turn + 1
      end
      ret = self:push("enemy_turn", params)
    else
      --players turn
      self.next_turn = self.next_turn % #self.turns + 1 --update counter
      ret = self:push("player_turn", params)
    end

    return ret
  else --current character is dead
    self.next_turn = self.next_turn % #self.turns + 1 --update counter
  end
end

function EncounterState:resume(params)
  print(params.action)
  if params.action == "Run" then
    if self.runnable then
      return self:pop({action = "continue_quest"})
    else
      self:view():get("message"):set("You can't run!")
      self.next_turn = self.next_turn - 1
    end
  end
end

return EncounterState
