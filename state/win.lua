local Vec = require "common.vec"
local MessageBox = require "view.message_box"
local State = require "state"

local WinState = require "common.class"(State)

function WinState:_init(stack)
  self:super(stack)

  self.message = nil
  self.wait = 4
end

function WinState:enter(params)
  local message = MessageBox(Vec(64, 64))

  params = params or {}

  self.wait = params.wait or self.wait

  self:view():add("message", message)

  message:set("Quest Complete! You Win!")
  message:center()
end

function WinState:leave()
  self:view():remove("message")
end

function WinState:update(dt)
  self.wait = self.wait - dt

  if self.wait <= 0 then
    self:pop({action = "end_quest"})
  end
end

return WinState
