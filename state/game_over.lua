local Vec = require "common.vec"
local MessageBox = require "view.message_box"
local State = require "state"

local GameOverState = require "common.class"(State)

function GameOverState:_init(stack)
  self:super(stack)

  self.message = nil
  self.wait = 4
end

function GameOverState:enter(params)
  local message = MessageBox(Vec(64, 64))

  params = params or {}

  self.wait = params.wait or self.wait

  self:view():add("message", message)

  message:set("Game Over")
  message:center()
end

function GameOverState:leave()
  self:view():remove("message")
end

function GameOverState:update(dt)
  self.wait = self.wait - dt

  if self.wait <= 0 then
    self:pop({action = "end_quest"})
  end
end

return GameOverState
