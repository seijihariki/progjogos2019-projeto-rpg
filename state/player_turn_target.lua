local Vec = require "common.vec"
local map = require "common.map"
local CharacterStats = require "view.character_stats"
local TurnCursor = require "view.turn_cursor"
local ListMenu = require "view.list_menu"
local State = require "state"

local PlayerTurnTargetState = require "common.class"(State)

function PlayerTurnTargetState:_init(stack)
  self:super(stack)
  self.character = nil
  self.menu = nil
  self.amount = nil
  self.remaining = nil
  self.selected = nil
end

function PlayerTurnTargetState:enter(params)
  self.character = params.current_character
  self.targets = params.targets
  self.amount = params.amount or 1
  self.remaining = self.amount
  self.selected = {}

  local menu_items =
    map(
    self.targets,
    function(target)
      return target:get_name()
    end
  )

  if self.amount < 0 then
    menu_items = {"All"}
    for i = 1, #self.targets do
      table.insert(self.selected, i)
    end
  end

  table.insert(menu_items, " Back")

  self.menu = ListMenu(menu_items)

  self:_show_menu()
  self:_show_target_cursor()
  self:_show_selected_cursors()
end

function PlayerTurnTargetState:_show_menu()
  local bfbox = self:view():get("battlefield").bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add("menu", self.menu)
end

function PlayerTurnTargetState:_show_target_cursor()
  if self.amount > 0 then
    self:view():add("target_cursor", self:_get_target_cursor(self.menu:current_option()))
  end
end

function PlayerTurnTargetState:_get_target_cursor(index)
  local atlas = self:view():get("atlas")
  local sprite_instance = atlas:get(self.targets[index])

  if self.amount > 1 then
    return TurnCursor(sprite_instance, "blue", "line")
  else
    return TurnCursor(sprite_instance, "red")
  end
end

function PlayerTurnTargetState:_show_selected_cursors()
  local atlas = self:view():get("atlas")

  for _, v in ipairs(self.selected) do
    local sprite_instance = atlas:get(self.targets[v])
    local cursor = TurnCursor(sprite_instance, "red")

    self:view():add("target_cursor_" .. v, cursor)
  end
end

function PlayerTurnTargetState:_show_stats()
  local bfbox = self:view():get("battlefield").bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add("char_stats", char_stats)
end

function PlayerTurnTargetState:leave()
  self:view():remove("target_menu")
  for _, v in ipairs(self.selected) do
    self:view():remove("target_cursor_" .. v)
  end

  if self.amount - 1 then
    self:view():remove("target_cursor")
  end
end

function PlayerTurnTargetState:on_keypressed(key)
  if key == "down" then
    self.menu:next()
  elseif key == "up" then
    self.menu:previous()
  elseif key == "return" then
    -- Go back if last option is selected
    if self.menu:current_option() > #self.targets then
      return self:pop(nil)
    end

    self.remaining = self.remaining - 1

    if self.amount > 0 then
      table.insert(self.selected, self.menu:current_option())
    end

    if self.remaining < 1 then
      return self:pop(
        {
          selected = map(
            self.selected,
            function(index)
              return self.targets[index]
            end
          )
        }
      )
    end

    self:_show_selected_cursors()
  end

  if self.amount > 0 then
    self:_show_target_cursor()
  end
end

return PlayerTurnTargetState
