local Character = require "model.character"
local State = require "state"

local FollowQuestState = require "common.class"(State)

function FollowQuestState:_init(stack)
  self:super(stack)
  self.party = nil
  self.events = nil
  self.next_event = nil
end

function FollowQuestState:enter(params)
  local quest = params.quest
  self.events = quest.events
  self.next_event = 1

  self.party = {}
  for i, character_name in ipairs(quest.party) do
    local character_spec = require("database.characters." .. character_name)
    self.party[i] = Character(character_spec)
  end
end

function FollowQuestState:update(_)
  if self.next_event <= #self.events then
    local event = self.events[self.next_event]

    if event.type == "encounter" then
      local encounter = {}

      for i, character_name in ipairs(event.enemies) do
        local character_spec = require("database.characters." .. character_name)
        encounter[i] = Character(character_spec)
      end

      local params = {party = self.party, encounter = encounter, runnable = event.runnable}

      return self:push("encounter", params)
    elseif event.type == "story" then
      local params = {
        text = event.text
      }

      return self:push("story", params)
    end
  else
    return self:push("win")
  end
end

function FollowQuestState:resume(params)
  if params.action == "game_over" then
    self:push("game_over")
  elseif params.action == "end_quest" then
    self:pop()
  elseif params.action == "continue_quest" then
    self.next_event = self.next_event + 1
  end
end

return FollowQuestState
