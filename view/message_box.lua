local Vec = require "common.vec"
local MessageBox = require "common.class"()

function MessageBox:_init(position)
  self.message = nil
  self.position = Vec(position:get())
  self.font = love.graphics.newFont("assets/fonts/VT323-Regular.ttf", 36)
  self.font:setFilter("nearest", "nearest")
end

function MessageBox:set(message)
  self.message = love.graphics.newText(self.font, message)
end

function MessageBox:set_position(position)
  self.position = Vec(position:get())
end

function MessageBox:center()
  self:center_h()
  self:center_v()
end

function MessageBox:center_h()
  local w = self:get_width()
  local windowW = love.graphics.getWidth()

  self.position.x = windowW / 2 - w / 2
end

function MessageBox:center_v()
  local h = self:get_height()
  local windowH = love.graphics.getHeight()
  self.position.y = windowH / 2 - h / 2
end

function MessageBox:get_width()
  return self.message:getWidth()
end

function MessageBox:get_height()
  return self.message:getHeight()
end

function MessageBox:draw()
  if self.message then
    local g = love.graphics
    g.push()
    g.setColor(1, 1, 1)
    g.translate(self.position:get())
    g.draw(self.message)
    g.pop()
  end
end

return MessageBox
