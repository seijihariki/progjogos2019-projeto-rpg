local Vec = require "common.vec"
local TurnCursor = require "common.class"()

local PALETTE_DB = require "database.palette"

function TurnCursor:_init(selected_drawable, color, style)
  self.offset = Vec(0, -55)
  self.selected_drawable = selected_drawable
  self.color = color or "full_white"
  self.style = style or "fill"
end

function TurnCursor:draw()
  if self.selected_drawable then
    local g = love.graphics
    local position = self.selected_drawable.position + self.offset
    g.push()
    g.translate(position:get())
    g.setColor(PALETTE_DB[self.color])
    g.setLineWidth(2)
    g.polygon(self.style, -8, 0, 8, 0, 0, 8)
    g.pop()
  end
end

return TurnCursor
